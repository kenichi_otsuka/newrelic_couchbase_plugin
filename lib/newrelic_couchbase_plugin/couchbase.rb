require 'net/http'
require 'json'

BASE_URI   = "/pools/default/buckets/"
#BUCKET_STAT_URI = "/pools/default/buckets/default/stats"

def getBucketInfo(host, port, bucket)

  Net::HTTP.start(host, port) do |http|

    # get bucket info
    begin
      request = Net::HTTP::Get.new BASE_URI + bucket + '/stats'
      response = http.request request
      data = JSON.parse(response.body)
    rescue
      return nil
    end

    evictions = data['op']['samples']['evictions']
    total_evictions = evictions.inject { |sum, val| sum + val }

    ops = data['op']['samples']['ops']
    total_ops = ops.inject { |sum, val| sum + val }

    # get mem info
    begin
      request = Net::HTTP::Get.new BASE_URI + bucket
      response = http.request request
      data = JSON.parse(response.body)
    rescue
      return nil
    end

    hitrate = data["basicStats"]["hitRatio"]
    used = data["basicStats"]["memUsed"]
    total =  data["quota"]["ram"]
    
    used_memory_rate = used.to_f / total.to_f * 100

    return {
      :evictions => total_evictions,
      :operations => total_ops,
      :used_memory_rate => used_memory_rate,
      :hit => hitrate
    }

  end
end
