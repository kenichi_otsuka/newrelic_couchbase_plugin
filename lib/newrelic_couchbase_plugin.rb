#! /usr/bin/env ruby

require "optparse"
require "rubygems"
require "bundler/setup"
require "newrelic_plugin"
require "newrelic_couchbase_plugin/version"
require "newrelic_couchbase_plugin/couchbase"

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: newrelic_couchbase_plugin [options]"
  opts.on("-c", "--config [FILE]", "specify config file") do |file|
    options[:config] = file 
  end
end.parse!

# set config file 
NewRelic::Plugin::Config.config_file = options[:config] if (options[:config])

#
#
# definition of New Relic Agent Module
#
#
module CouchbaseAgent

  class Agent < NewRelic::Plugin::Agent::Base

    agent_guid "com.zalora.newrelic.couchbase"
    agent_version CouchbaseAgent::VERSION

    #
    # retrieve configs
    agent_config_options :label, :bucket, :host, :port

    #
    # setup label data
    agent_human_labels("Couchbase") { label }

    def poll_cycle
      info = getBucketInfo(host, port, bucket)
      report_metric "evictions", "Value", info[:evictions]
      report_metric "operations", "Value", info[:operations]
      report_metric "used_memoey_rate", "Value", info[:used_memory_rate]
    end

  end

end

#
# Register this agent with the component.
#
NewRelic::Plugin::Config.config.agents.each { |key, value|
 NewRelic::Plugin::Setup.install_agent key.to_s, CouchbaseAgent
}

#
# Launch the agent; this never returns.
#
NewRelic::Plugin::Run.setup_and_run
