# -*- encoding: utf-8 -*-
require File.expand_path('../lib/newrelic_couchbase_plugin/version', __FILE__)

Gem::Specification.new do |s|
  s.specification_version = 2 if s.respond_to? :specification_version=
  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.rubygems_version = '1.3.5'

  ## Leave these as is they will be modified for you by the rake gemspec task.
  s.name          = 'newrelic_couchbase_plugin'
  s.version       = CouchbaseAgent::VERSION
  s.rubyforge_project = 'newrelic_couchbase_plugin'

  ## Edit these as appropriate
  s.summary     = "New Relic Couchbase Plugin"
  s.description   = %q{The New Relic Plugin for Couchbase}

  s.authors       = ["Kenichi Otsuka"]
  s.email         = ["kenichi.otsuka@zalora.com"]
  s.homepage =    'https://bitbucket.org/kenichi_otsuka/newrelic_couchbase_plugin'

  s.require_paths = %w[lib]

  s.rdoc_options = ["--charset=UTF-8",
                    "--main", "README.md"]
  s.extra_rdoc_files = %w[LICENSE]

  s.add_dependency 'bundler'
  s.add_dependency 'newrelic_plugin', '>= 1.0.0'

  s.files         = `git ls-files`.split($\)
  s.executables   = s.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  s.test_files    = s.files.grep(%r{^(test|spec|features)/})


  ## Test files will be grabbed from the file list. Make sure the path glob
  ## matches what you actually use.
  s.test_files = s.files.select { |path| path =~ /^test\/test_.*\.rb/ }

end
